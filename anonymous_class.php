<?php
interface Logger {
    public function log($message);
}

// class TerminalLogger implements Logger {
//     public function log($message) {
//         var_dump($message);
//     }
// }

class Application {

    protected $logger;

    public function setLogger($logger) {
        $this->logger = $logger;
        return $this;
    }

    public function action() {
        $this->logger->log('I logged it here !');
    }
}

$app = new Application();
//$app->setLogger(new TerminalLogger);
$app->setLogger(new class implements Logger {
    public function log($message) {
        var_dump($message);
    }
});
$app->action();
