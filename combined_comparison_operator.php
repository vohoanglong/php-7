<?php
$a = 10;
$b = 20;

// Normally
echo ($a < $b) ? -1 : (($a > $b ) ? 1 : 0) . "\n";
echo "\n";

// Combined comparision
echo $a <=> $b;
echo "\n";

$a = 80;
$b = 60;

// Normally
echo ($a < $b) ? -1 : (($a > $b ) ? 1 : 0);
echo "\n";

// Combined comparision
echo $a <=> $b;
echo "\n";

$a = 50;
$b = 50;

// Normally
echo ($a < $b) ? -1 : (($a > $b ) ? 1 : 0);
echo "\n";

// Combined comparision
echo $a <=> $b;
echo "\n";
