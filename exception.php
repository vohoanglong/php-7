<?php
function call_me($object) {
    $object->method();
}

try {
    call_me(null);
} catch (EngineException $e) {
    echo 'Excption ' . $e->getMessage() . "\n";
}
