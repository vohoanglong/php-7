<?php
$x = NULL;
$y = NULL;
$z = 3;

$result = !empty($x) ? $x : (!empty($y) ? $y : (!empty($z) ? $z : 'empty'));

echo $result . "\n";

// Using in PHP 7
$result2 = $x ?? $y ?? $z;

echo $result2 . "\n";
