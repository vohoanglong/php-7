<?php
interface Vendor {
    public function make() :string;
}

class Samsung implements Vendor{
    public function make() :string {
        return "This is Samsung product";
    }
}

$vendor = new Samsung();
echo $vendor->make();
