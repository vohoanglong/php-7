<?php
class Samsung {
    public function make() :string {
        return "This is Samsung product";
    }
}

class Galaxy extends Samsung {
    public function make() {
        return new Samsung();
    }
}

$vendor = new Galaxy();
echo $vendor->make();
