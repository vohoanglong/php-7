<?php
class Person {
    public $name = 'Jackie';
    public $job = 'PHP Developer';
}

// Create instance
$person = new Person();
$property = ['first' => 'name', 'second' => 'job'];

echo "\nMy name is " . $person->{$property['first']} . "\n";
echo "\nMy job is " . $person->$property['second'] . "\n";
