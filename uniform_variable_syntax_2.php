<?php
class Person {
    public static $company = 'TechBase VietNam';
    public function getFriends() {
        return [
            'John' => function () {
                return 'John is my best friend!';
            },
            'Alan' => function () {
                return 'Alan is old friend!';
            }
        ];
    }

    public function getFriendsOf($someone) {
        return $this->getFriends()[$someone]();
    }

    public static function getNewPerson() {
        return new Person();
    }
}

// Create instance
$person = new Person();
// Get friend
echo "\n" . $person->getFriends()['John']() . "\n";
echo "\n" . $person->getFriendsOf('Alan') . "\n";

// Get company
echo "\n" . $person::getNewPerson()::$company . "\n";
